package ru.t1consulting.nkolesnik.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class VersionListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Show application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@versionListener.getName() == #event.name || " +
            "@versionListener.getArgument() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[VERSION]");
        @Nullable final String version = getSystemEndpoint().getVersion(new ServerVersionRequest()).getVersion();
        System.out.println(version);
    }

}
