package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;

import java.util.List;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

    @Nullable
    @Query("SELECT p FROM ProjectDto p WHERE p.id=:id AND p.userId=:userId")
    ProjectDto findById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @NotNull
    @Query("SELECT p FROM ProjectDto p WHERE p.userId=:userId")
    List<ProjectDto> findAll(
            @Nullable @Param("userId") final String userId
    );

    @NotNull
    @Query("SELECT p FROM ProjectDto p WHERE p.userId=:userId ORDER BY :sortOrder")
    List<ProjectDto> findAll(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("sortOrder") final String sortOrder
    );

    @Query("SELECT COUNT(1) = 1 FROM ProjectDto p WHERE p.id=:id AND p.userId=:userId")
    boolean existsById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Query("SELECT COUNT(p) FROM ProjectDto p WHERE p.userId=:userId")
    long getSize(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM ProjectDto p WHERE p.userId=:userId AND p.id=:id")
    void removeById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Modifying
    @Query("DELETE FROM ProjectDto p WHERE p.userId=:userId")
    void clear(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("UPDATE ProjectDto p SET p.status=:status WHERE p.id=:id AND p.userId=:userId")
    void changeProjectStatusById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id,
            @Nullable @Param("status") final String status
    );

}
