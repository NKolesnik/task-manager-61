package ru.t1consulting.nkolesnik.tm.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1consulting.nkolesnik.tm.listener.LoggerListener;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1consulting.nkolesnik.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    @NotNull
    public LoggerListener loggerListener() {
        return new LoggerListener();
    }

}